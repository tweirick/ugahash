"""
This program accepts
"""
from __future__ import print_function
import sys

#TCONS_00065868	NCBI_105:XM_005269970.1	NCBI_103:NM_006258.3	NCBI_105:NM_006258.3	NCBI_104:NM_006258.3    ...
overlap_cnt = {}
for line in open(sys.argv[1]):

    overlap_set = "-".join(sorted(set(x.split("_")[0] for x in line.split()[1:])))

    if not overlap_set in overlap_cnt:
        overlap_cnt.update({overlap_set: 0})
    overlap_cnt[overlap_set] += 1 

for venn_section in sorted(overlap_cnt):
    print( venn_section + "\t"  + str(overlap_cnt[venn_section]) )

