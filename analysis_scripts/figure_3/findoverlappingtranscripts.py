"""
This program will search for overlapping sequences
This program is made to work on the output of cuffcompare that has had transcript lines added.
"""
from __future__ import print_function
import sys
import MySQLdb
import re

"""
Connect to UGAHash database.
"""
HOST = "localhost"
USER = "root"
PASSWORD = open("/home/tyler/.dbpassword").read().strip()
DB = "ugahash"

db = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWORD, db=DB)
c = db.cursor()

for line in open(sys.argv[1]):
    #1	Cufflinks	transcript	6649409	6650034	.	+	.	gene_id "XLOC_000114"; transcript_id "TCONS_00047063";
    spln = line.split()
    if spln[2] == "transcript":
        #If cufflinks transcript overlaps
        transcript_id = re.findall("transcript_id \"(.*?)\";", line)[0]
        chromosome = spln[0]
        start = int(spln[3])
        stop = int(spln[4])
        strand = spln[6]

        c.execute(
            """
            SELECT source_version_id_id, original_id FROM hasher_sequence
            JOIN hasher_sequencesetsequencerelation ON hash_id_id = hash_id
            WHERE
            hasher_sequence.assembly = "hg19" AND
            seq_type = "transcript" AND
            chromosome = %s AND
            strand     = %s AND
            (
                start = %s OR stop = %s OR
                start BETWEEN %s AND %s OR
                stop  BETWEEN %s AND %s OR
                %s BETWEEN start AND stop OR %s BETWEEN start AND stop
            );
            """, (
                  chromosome, strand,
                  start, stop,
                  start, stop,
                  start, stop,
                  start, stop,
            )
        )

        source_version_id_id = c.fetchall()
        data = [x[0]+":"+x[1] for x in source_version_id_id]
        if len(data) > 0:
            print(transcript_id+"\t"+"\t".join(data))

