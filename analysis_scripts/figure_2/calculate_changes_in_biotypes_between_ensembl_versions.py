"""
This program will calculate the number of newly added transcripts and removed transcripts between ENSEMBL versions and
print the results.
"""
from __future__ import print_function
import argparse
import json

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--gtf', dest='gtf', nargs='+', required=True, help="Pass a regex describing a set of GFF files.")
parser.add_argument('--biotypes', dest='biotypes', required=True, help="Biotypes, one per line in a text file.")
args = parser.parse_args()

"""
Generate set of biotypes.
"""
biotype_set = set()
for line in open(args.biotypes):
    biotype_set.add(line.strip())

"""
Print title.
"""
print("\t".join(["title", "removed_ids", "new_ids", "common_ids"]))

"""
Iterate over give gff files.
"""
prev_transcript_id_set = set()
for gtf_json in args.gtf:

    """
    This program uses JSON representations of GFF files. The code below handles parsing the JSON file.
    """
    json_schema = open(gtf_json)
    json_obj = json.load(json_schema)
    gene_dict = json_obj['gene']

    """
    Iterate through genes and their corresponding transcripts. Creating a set of all transcript ids.
    """
    transcript_id_set = set()
    for gene_id in gene_dict:

        gene_obj = gene_dict[gene_id]

        for transcript_id in gene_obj['transcript']:

            t_obj = gene_obj['transcript'][transcript_id]

            if t_obj['transcript_biotype'] in biotype_set:

                transcript_id_set.add(transcript_id)

    """
    Use set operations to find the amounts of new, removed, and common ids between two assembly versions.
    """
    new_ids = transcript_id_set - prev_transcript_id_set
    removed_ids = prev_transcript_id_set - transcript_id_set
    common_ids = transcript_id_set & prev_transcript_id_set

    """
    Finally prepare for next iteration.
    """
    prev_transcript_id_set = transcript_id_set

    """
    Print results.
    """
    print(gtf_json, end="\t")
    print(-1 * len(removed_ids), end="\t")
    print(len(new_ids), end="\t")
    print(len(common_ids))



