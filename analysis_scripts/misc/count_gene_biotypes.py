from __future__ import print_function
import re

base_dir = "/media/ATLAS_NGS_storage/Tyler/UGAHash/data/2015-06-10_1_GTF_and_GFF_files/ensembl/"

trans_dict = {
    'unitary_pseudogene',
    'rRNA',
    'lincRNA',
    'IG_pseudogene',
    'translated_processed_pseudogene',
    'protein_coding',
    'snoRNA_pseudogene',
    'antisense',
    'snRNA_pseudogene',
    'IG_V_gene', 'polymorphic_pseudogene', 'misc_RNA', 'TR_D_gene',
    'misc_RNA_pseudogene', 'IG_J_gene', 'TR_J_pseudogene',
    'IG_J_pseudogene',
    'scRNA',
    'IG_C_gene',
    'Mt_tRNA',
    'Mt_rRNA', 'havana_pseudogene', 'TR_V_pseudogene', '3prime_overlapping_ncrna', 'nonsense_mediated_decay',
    'TR_J_gene', 'transcribed_processed_pseudogene', 'scRNA_pseudogene', 'IG_V_pseudogene', 'TEC', 'pseudogene',
    'snRNA', 'C_segment', 'unprocessed_pseudogene', 'TR_V_gene', 'J_segment', 'tRNA_pseudogene',
    'rRNA_pseudogene', 'sense_intronic', 'V_segment', 'miRNA', 'IG_C_pseudogene',
    'transcribed_unprocessed_pseudogene', 'non_coding', 'ambiguous_orf', 'sense_overlapping',
    'IG_D_gene', 'LRG_gene', 'ncrna_host', 'disrupted_domain', 'TR_C_gene', 'retained_intron',
    'processed_transcript', 'retrotransposed', 'Mt_tRNA_pseudogene', 'snoRNA', 'miRNA_pseudogene',
    'non_stop_decay', 'processed_pseudogene', 'D_segment'
}

#http://www.gencodegenes.org/gencode_biotypes.html
#http://vega.sanger.ac.uk/info/about/gene_and_transcript_types.html

trans_dict = {


'LRG_gene':         "misc",
'disrupted_domain': "misc",
'TEC':              'misc',


'C_segment': "protein_coding",
'J_segment': "protein_coding",
'V_segment': "protein_coding",
'D_segment': "protein_coding",
'protein_coding':          'protein_coding',
'IG_C_gene':               'protein_coding',
'IG_D_gene':               'protein_coding',
'IG gene':                 'protein_coding',
'IG_J_gene':               'protein_coding',
'IG_LV_gene':              'protein_coding',
'IGM gene':                'protein_coding',
'IG_V_gene':               'protein_coding',
'IGZ gene':                'protein_coding',
'nonsense mediated decay': 'protein_coding',
'nonsense_mediated_decay': 'protein_coding',
'nontranslating CDS':      'protein_coding',
'non stop decay':          'protein_coding',
'non_stop_decay':          'protein_coding',
'TR_C_gene':               'protein_coding',
'TR_D_gene':               'protein_coding',
'TR_J_gene':               'protein_coding',
'TR_V_gene':               'protein_coding',


'retrotransposed':  "pseudogene",
'pseudogene':                         'pseudogene',
'polymorphic pseudogene':             'pseudogene',
'polymorphic_pseudogene':             'pseudogene',
'disrupted domain':                   'pseudogene',
'IG_C_pseudogene':                    'pseudogene',
'IG_J_pseudogene':                    'pseudogene',
'IG_pseudogene':                      'pseudogene',
'IG_V_pseudogene':                    'pseudogene',
'TR_V_pseudogene':                    'pseudogene',
'processed pseudogene':               'pseudogene',
'processed_pseudogene':               'pseudogene',
'transcribed_processed_pseudogene':   'pseudogene',
'transcribed processed pseudogene':   'pseudogene',
'transcribed unitary pseudogene':     'pseudogene',
'transcribed_unitary_pseudogene':     'pseudogene',
'unitary_pseudogene':                 'pseudogene',
'transcribed_unprocessed_pseudogene': 'pseudogene',
'translated_unprocessed_pseudogene':  'pseudogene',
'translated_processed_pseudogene':    'pseudogene',
'TR_J_pseudogene':                    'pseudogene',
'unprocessed_pseudogene':             'pseudogene',

'snoRNA_pseudogene':   "pseudogene",
'snRNA_pseudogene':    "pseudogene",
'misc_RNA_pseudogene': "pseudogene",
'havana_pseudogene':   "pseudogene",
'scRNA_pseudogene':    "pseudogene",
'rRNA_pseudogene':     "pseudogene",
'Mt_tRNA_pseudogene':  "pseudogene",


'3prime_overlapping_ncrna': 'lncRNA',
'ambiguous orf':            'lncRNA',
'ambiguous_orf': "lncRNA",

'antisense':                'lncRNA',
'antisense RNA':            'lncRNA',
'lincRNA':                  'lncRNA',
'ncrna_host': "lncRNA",
'ncrna host':               'lncRNA',
'processed transcript':     'lncRNA',
'processed_transcript':     'lncRNA',
'sense intronic':           'lncRNA',
'sense_intronic':           'lncRNA',
'sense_overlapping':        'lncRNA',
'retained_intron':          'lncRNA',
'retained intron':          'lncRNA',
'non_coding':               'lncRNA',
'non coding':               'lncRNA',
'macro_lncRNA':             'lncRNA',

'known_ncrna':      'ncRNA',
'miRNA':              'ncRNA',
'miRNA_pseudogene':   'ncRNA',
'miscRNA':            'ncRNA',
'misc_RNA':           'ncRNA',
'miscRNA pseudogene': 'ncRNA',
'Mt_rRNA':            'ncRNA',
'Mt_tRNA':            'ncRNA',
'rRNA':               'ncRNA',
'scRNA':              'ncRNA',
'snlRNA':             'ncRNA',
'snoRNA':             'ncRNA',
'snRNA':              'ncRNA',
'tRNA':               'ncRNA',
'tRNA_pseudogene':    'ncRNA',
'vaultRNA':           "ncRNA",
'scaRNA':             'ncRNA',
'sRNA':               'ncRNA',
'ribozyme':         'ncRNA',


}

gene_biotype_counts = {}
gene_biotype_set = set()

gtf_files = """Homo_sapiens.NCBI36.49.gtf
Homo_sapiens.NCBI36.50.gtf
Homo_sapiens.NCBI36.51.gtf
Homo_sapiens.NCBI36.52.gtf
Homo_sapiens.NCBI36.53.gtf
Homo_sapiens.NCBI36.54.gtf
Homo_sapiens.GRCh37.55.gtf
Homo_sapiens.GRCh37.56.gtf
Homo_sapiens.GRCh37.57.gtf
Homo_sapiens.GRCh37.58.gtf
Homo_sapiens.GRCh37.59.gtf
Homo_sapiens.GRCh37.60.gtf
Homo_sapiens.GRCh37.61.gtf
Homo_sapiens.GRCh37.62.gtf
Homo_sapiens.GRCh37.63.gtf
Homo_sapiens.GRCh37.64.gtf
Homo_sapiens.GRCh37.65.gtf
Homo_sapiens.GRCh37.66.gtf
Homo_sapiens.GRCh37.67.gtf
Homo_sapiens.GRCh37.68.gtf
Homo_sapiens.GRCh37.69.gtf
Homo_sapiens.GRCh37.70.gtf
Homo_sapiens.GRCh37.71.gtf
Homo_sapiens.GRCh37.72.gtf
Homo_sapiens.GRCh37.73.gtf
Homo_sapiens.GRCh37.74.gtf
Homo_sapiens.GRCh37.75.gtf
Homo_sapiens.GRCh38.76.gtf"""

for file_name in gtf_files.split("\n"):
    name = file_name.split(".")[-2]
    gene_biotype_counts.update({name: {}})
    for line in open(base_dir+file_name):
        spln = line.split("\t")

        if len(spln) > 2 and spln[2] == "exon" and 'exon_number "1";' in line:
            biotype = trans_dict[spln[1]]
            gene_biotype_set.add(biotype)

            if not biotype in gene_biotype_counts[name]:
                gene_biotype_counts[name].update({biotype: 0})
            gene_biotype_counts[name][biotype] += 1


#print(gene_biotype_set)


gtf_files = """Homo_sapiens.GRCh38.77.gtf
Homo_sapiens.GRCh38.78.gtf
Homo_sapiens.GRCh38.79.gtf
Homo_sapiens.GRCh38.80.gtf
Homo_sapiens.GRCh38.81.gtf"""

for file_name in gtf_files.split("\n"):
    name = file_name.split(".")[-2]
    gene_biotype_counts.update({name: {}})
    for line in open(base_dir+file_name):
        spln = line.split("\t")
        if len(spln) > 2 and spln[2] == "exon" and 'exon_number "1";' in line:

            biotype = trans_dict[re.findall('gene_biotype \"(\w+)\"', line)[0]]
            biotype = trans_dict[re.findall('transcript_biotype \"(\w+)\"', line)[0]]


            gene_biotype_set.add(biotype)

            if not biotype in gene_biotype_counts[name]:
                gene_biotype_counts[name].update({biotype: 0})
            gene_biotype_counts[name][biotype] += 1


sorted_biotypes = sorted(gene_biotype_set)

print("Ensembl_Version\t"+"\t".join(sorted_biotypes), end="")

for name in sorted(gene_biotype_counts):
    print("\n"+name, end="\t")

    for biotype in sorted_biotypes:
        if biotype in gene_biotype_counts[name]:
            print(gene_biotype_counts[name][biotype], end="\t")
        else:
            print(0, end="\t")
