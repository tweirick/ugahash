import sys

map_file = sys.argv[1]
seqset_file = sys.argv[2]
id_maps = {}

for line in open(map_file):
    spln = line.split()
    id_maps[spln[0]] = spln[1]

for line in open(seqset_file):
    spln = line.strip().split("\t")

    if spln[1] in id_maps:
        print("\t".join([spln[0], id_maps[spln[1]], spln[2], spln[3]]))
    else:
        print("\t".join([spln[0], spln[1], spln[2], spln[3]]))