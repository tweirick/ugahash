import re
import sys

#regex_match = re.findall(regex_choices[choice_cnt], line)
#regex_match = re.findall(regex_choices[choice_cnt], line)

for line in open(sys.argv[1]):

    if line.split("\t")[2] == "transcript":
        id = re.findall("ID=(\\w+);", line)

        gene_id = re.findall("transcript_id=(\\w+\\.+\\w*)", line)

        if not gene_id:
            gene_id = re.findall("Genbank:(\\w+\\.+\\w*)", line)

        if not gene_id:
            gene_id = re.findall("miRBase:(\\w+)", line)

        #print line
        if gene_id:
            #print(id)
            #print(gene_id)

            print(id[0]+"\t"+gene_id[0])
