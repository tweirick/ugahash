from __future__ import print_function
import MySQLdb
import sys
DB     = "ugahash"
HOST   = "localhost"
USER   = "root"
PASSWD = open("/home/tyler/.dbpassword").read().strip()
db     = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD, db=DB)
c      = db.cursor()

c.execute("""SELECT DISTINCT source FROM hasher_sequenceset ORDER BY source DESC;""")
sources = [i[0] for i in c.fetchall()]

seq_type = sys.argv[1]

for el in sources: 

	c.execute(""" 
	SELECT COUNT(*) FROM (
	SELECT DISTINCT hash_id FROM (
	SELECT hasher_sequence.hash_id as hash_id
	FROM hasher_sequence 
	JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id 
	JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
	WHERE hasher_sequence.seq_type = %s AND hasher_sequenceset.source = %s)a 
	)c;
	""", (seq_type, el))
	cnt = str(c.fetchone()[0])
	print(el+"\t"+cnt)
	




