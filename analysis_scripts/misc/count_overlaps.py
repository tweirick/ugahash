import MySQLdb
DB     = "ugahash"
HOST   = "localhost"
USER   = "root"
PASSWD = open("/home/tyler/.dbpassword").read().strip()
db     = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD, db=DB)
c      = db.cursor()

c.execute("""SELECT DISTINCT source FROM hasher_sequenceset;""")
sources = [i[0] for i in c.fetchall()]

print(sources)


for el1 in sources:
	for el2 in sources: 

		if el1 != el2:

			c.execute(""" 
			SELECT COUNT(*) FROM (
			SELECT DISTINCT hash_id FROM (
			SELECT hasher_sequence.hash_id as hash_id
			FROM hasher_sequence 
			JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id 
			JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
			WHERE hasher_sequence.seq_type = "transcript" AND hasher_sequenceset.source = %s)a 
			INNER JOIN (
			SELECT hasher_sequence.hash_id as hash_id
			FROM hasher_sequence 
			JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id 
			JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
			WHERE hasher_sequence.seq_type = "transcript" AND hasher_sequenceset.source = %s)b 
			USING (hash_id) 
			)c;
			""", (el1, el2, ))
			cnt = str(c.fetchone()[0])
			print(el1+"-"+el2+"\t"+cnt)
		

		
		for el3 in sources:
			if el1 != el2 and el1 != el3 and el2 != el3:
				c.execute(""" 
				SELECT COUNT(*) FROM (
				SELECT DISTINCT hash_id FROM (
				SELECT hasher_sequence.hash_id as hash_id
				FROM hasher_sequence 
				JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id 
				JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
				WHERE hasher_sequence.seq_type = "transcript" AND hasher_sequenceset.source = %s)a 
				INNER JOIN (
				SELECT hasher_sequence.hash_id as hash_id
				FROM hasher_sequence 
				JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id 
				JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
				WHERE hasher_sequence.seq_type = "transcript" AND hasher_sequenceset.source = %s)b 
				USING (hash_id) 
				INNER JOIN (
				SELECT hasher_sequence.hash_id as hash_id
				FROM hasher_sequence 
				JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id 
				JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
				WHERE hasher_sequence.seq_type = "transcript" AND hasher_sequenceset.source = %s)d 
				USING (hash_id)
				)c;
				""", (el1, el2, el3, ))
				cnt = str(c.fetchone()[0])
				print(el1+"-"+el2+"-"+el3+"\t"+cnt)

#			for el4 in sources: 



