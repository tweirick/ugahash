from __future__ import print_function
import MySQLdb
import sys
DB     = "ugahash"
HOST   = "localhost"
USER   = "root"
PASSWD = open("/home/tyler/.dbpassword").read().strip()
db     = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD, db=DB)
c      = db.cursor()

c.execute("""SELECT DISTINCT source FROM hasher_sequenceset ORDER BY source DESC;""")
sources = [i[0] for i in c.fetchall()]

seq_type = sys.argv[1]

el1 = sources.pop()
print("x\t"+"\t".join(sources))
while sources:

    for el2 in sources:
        print(el1+" "+el2)

        c.execute("""
        SELECT hash_id FROM (
        SELECT DISTINCT hash_id FROM (
        SELECT hasher_sequence.hash_id as hash_id
        FROM hasher_sequence
        JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id
        JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
        WHERE hasher_sequence.seq_type = %s AND hasher_sequenceset.source = %s)a
        INNER JOIN (
        SELECT hasher_sequence.hash_id as hash_id
        FROM hasher_sequence
        JOIN hasher_sequencesetsequencerelation ON hasher_sequencesetsequencerelation.hash_id_id           = hasher_sequence.hash_id
        JOIN hasher_sequenceset                 ON hasher_sequencesetsequencerelation.source_version_id_id = hasher_sequenceset.source_version_id
        WHERE hasher_sequence.seq_type = %s AND hasher_sequenceset.source = %s)b
        USING (hash_id)
        )c;
        """, (seq_type, el1, seq_type, el2,))

        of = open(seq_type+"_"+el1+"_"+el2, 'w')
        for el in c.fetchall():
            of.write(el[0]+"\n")
        of.close()
    el1 = sources.pop()


