from pylab import *
import argparse



ax = axes()

parser = argparse.ArgumentParser()
parser.add_argument('--infile',        dest='infile',        required=True,  help="")
parser.add_argument('--xlabel',        dest='xlabel',        required=False, default="", help="")
parser.add_argument('--ylabel',        dest='ylabel',        required=False, default="", help="")
parser.add_argument('--percent_based', dest='percent_based', required=False, default=False, help="")
#parser.add_argument('--colrows', dest='percent_based', required=False, default=False, help="")

#ax.set_ylim([-5, 5])

args = parser.parse_args()

file_name = args.infile
xlabel = args.xlabel
ylabel = args.ylabel
delim = "\t"

titles = []
data_list = []

for line in open(args.infile):
    spln = line.strip("\n").split(delim)

    titles.append(spln[0])

    values = spln[1:]
    if args.percent_based:
        data_list.append([round(float(i) * 100.0, 1) for i in values])
    else:
        data_list.append([float(i) for i in values])





#print "25th_percentile", "Average", "75th_percentile", 'median', 'stddev'
for i in range(len(data_list)):
    print(
        titles[i],
        #np.percentile(data_list[i], 25),
        #np.average(data_list[i]),
        #np.percentile(data_list[i], 75),
        #np.median(data_list[i]),
        #np.std(data_list[i]),
        #len(data_list[i])
    )

for el in titles:
    print el

ax.set_xticklabels(titles)
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)
ax.set_ylim([-5, 5])

bp = boxplot(data_list, sym='', widths=0.5)

hold(True)

setp(bp['boxes'],    color='black')
setp(bp['caps'],     color='black')
setp(bp['whiskers'], color='black')
setp(bp['fliers'],   color='black')
setp(bp['medians'],  color='black')

show()


