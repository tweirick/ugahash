import sys

convert_dict = {}
for line in open(sys.argv[1]):
    spln = line.split()
    convert_dict.update({spln[0]:spln[-1]})


with open(sys.argv[2]) as f:
    next(f)
    for line in f:
        spln = line.split()
        
        oid  = spln[0]
        length = spln[7]
        FPKM = spln[9]
        
        if oid in convert_dict:
            print( "\t".join([convert_dict[oid], length, "-", "-", FPKM]) )  
        

