import sys

print("target_id\tlength\teff_length\test_counts\ttpm")

with open(sys.argv[1]) as f:
    next(f)
    for line in f:
        spln = line.split() 
        oid    = spln[0]
        length = spln[7]
        FPKM   = spln[9]
        print( "\t".join([oid, length, "-", "-", FPKM]) )  
        
