"""
This program calculates
#map_file = "/media/Storage/UGAHash_assembly/mart_export_v79_mRNA.txt.calc_offset.tsv.e_and_n.tsv"
#map_file = "/media/Storage/UGAHash_assembly/matching_hashes_map_e79_n107.tsv.noidids.tsv.ensembl_ncbi.tsv"
#f1 = glob("/media/Storage/UGAHash_assembly/SRP019807/KALLISTO/nobootstrap/*.tsv")
#f2 = glob("/media/Storage/UGAHash_assembly/SRP019807/KALLISTOn/results/*.tsv")
#f1 = glob("/media/ATLAS_NGS_storage/Tyler/UGAHash/data/20151012_differences_in_rnaseqannsembly/cufflinks_data/ENSEMBL/SRR78727*.idconverted")
#f2 = glob("/media/ATLAS_NGS_storage/Tyler/UGAHash/data/20151012_differences_in_rnaseqannsembly/cufflinks_data/NCBI/SRR78727*.idconverted")
"""

import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--map_file', dest='map_file', required=True, help="")
parser.add_argument('--exp_f1',   dest='exp_f1', nargs='+',  required=True, help="")
parser.add_argument('--exp_f2',   dest='exp_f2', nargs='+', required=True, help="")
parser.add_argument('--titles', dest='titles', required=True, help="")
parser.add_argument('--out_file', dest='out_file', required=True, help="")
parser.add_argument('--remove_not_detected',  dest='remove_not_detected',  required=False, default=False, help="")
args = parser.parse_args()

assert args.out_file != args.map_file

f1_dict = {}
f2_dict = {}
id_map = []
id_dict = {}

# Populate accession list with zeros. If a sequence is not detected assume its expression is zero.
for line in open(args.map_file):
    spln = line.strip().split()
    f1_dict.update({spln[0]: 0})
    f2_dict.update({spln[1].split(".")[0]: 0})
    id_map.append([spln[0], spln[1].split(".")[0]])
    id_dict.update({spln[0]: spln[1].split(".")[0]})

# Kallisto's Data format.
# target_id	length	eff_length	est_counts	tpm
# ENST00000415118	8	3.8125	0	0
# ENST00000448914	13	7.8	0	0
# Add values from data set one.
f1_file_dict = {}
f1_len_change = {}
for file_name in sorted(args.exp_f1):
    file_key = file_name.split("/")[-1].split(".")[0]
    f1_file_dict[file_key] = dict()
    f1_len_change[file_key] = dict()
    for line in open(file_name):
        spln = line.strip().split()
        if spln[0] in f1_dict:
            #f1_file_dict[file_key][spln[0]] = spln[1:]
            f1_file_dict[file_key][spln[0]] = float(spln[-1])
            f1_len_change[file_key][spln[0]] = int(spln[1])

# Add values from data set two.
f2_file_dict = {}
f2_len_change = {}
for file_name in sorted(args.exp_f2):
    file_key = file_name.split("/")[-1].split(".")[0]
    f2_file_dict[file_key] = dict()
    f2_len_change[file_key] = dict()
    for line in open(file_name):
        spln = line.strip().split()
        row_id = spln[0].split(".")[0]
        if spln[0].split(".")[0] in f2_dict:
            #f2_file_dict[file_key][row_id] = spln[1:]
            f2_file_dict[file_key][row_id]  = float(spln[-1])
            f2_len_change[file_key][row_id] = int(spln[1])

arr_1 = []
arr_2 = []
for id in sorted(id_dict):
    tmp_arr_1, tmp_arr_2 = [], []
    for f_el in sorted(f1_file_dict):
        if id in f1_file_dict[f_el] and id_dict[id] in f2_file_dict[f_el]:
            tmp_arr_1.append(f1_file_dict[f_el][id])
            tmp_arr_2.append(f2_file_dict[f_el][id_dict[id]])
    if tmp_arr_1 != [] and tmp_arr_2 != []:
        arr_1.append(tmp_arr_1)
        arr_2.append(tmp_arr_2)

arr_1 = np.array(arr_1)
arr_2 = np.array(arr_2)

assert len(arr_1) == len(arr_2)

arr_dif = []
arr_dif_rows = [ [] for z in range(len(arr_1[0])) ]

for i in range(len(arr_1)):     

    assert len(arr_1[i]) == len(arr_2[i])

    if not args.remove_not_detected or (sum(arr_1[i]) != 0 and sum(arr_2[i]) != 0):
        tmp_d = np.array(arr_1[i]) - np.array(arr_2[i])
        arr_dif.append("\t".join(["%.4f" % x for x in tmp_d]))

        for j in range(len(arr_1[i])):
            if not (arr_1[i][j] == 0 and arr_2[i][j] == 0):
                arr_dif_rows[j].append("%.4f" % tmp_d[j])

print(len(arr_dif))

titles = []
for el in open(args.titles):
    titles.append(el.strip())

#arr_dif = arr_1 - arr_2
#arr_dif = np.array(arr_dif)
#of = open(args.map_file+".FPKM_change_matrix_3.tsv", 'w')
of = open(args.out_file+".cols", 'w')
of.write("\n".join(arr_dif))
of.close()


of = open(args.out_file+".rows", 'w')
of.write("\n".join([titles[i]+"\t"+"\t".join(arr_dif_rows[i]) for i in range(len(arr_dif_rows))]))
of.close()

#np.savetxt(map_file+".FPKM_change_matrix_1.tsv", arr_dif, delimiter="\t")
#for el in arr_dif:
#    print el
#    print "\t".join( list([str(round(i, 5) for i in el)]))
#