"""
This program will calculate the total length change
in transcripts with mismatching hashes.

transcript_1 transcript_2 difference_x difference_y total_len_change
0               1                               2   3           4           5               6
ENST00000544879	UT_GRCh38wWgCL5sUkRDG69_Ln36WJw	10	73110375	73131828	NM_001283016	UT_GRCh38xrC74Pt8qh4GvtP_T2jKFA	10	73110375	73131828	0	0	0
ENST00000375200	UT_GRCh38UQSKgQlcGHMTOyqcngtxJg	20	33731760	33792269	NM_001282933	UT_GRCh38LXjj_MKaNa7KYsmq5uyW-A	20	33731760	33792269	0	0	0
ENST00000372336	UT_GRCh382VrDzrgDcaQ9NGUiPNhzow	10	79382325	79445627	NM_153367	UT_GRCh38g3SYzKGr84FHND8cuYd0VQ	10	79382325	79445627	0	0	0
ENST00000373965	UT_GRCh385gC-BIqNNwRZpbNWlzARnA	10	53802771	54801291	NM_001142772	UT_GRCh38o1968TE_2ns6R0FcikSWHA	10	53802771	54801291	0	0	0
ENST00000277570	UT_GRCh38buu6-DiVtu2xzQv7X3jIYw	10	11823398	11872277	NM_153256	UT_GRCh38aPd_GrsPu-DciaeM5708ow	10	11823398	11872277	0	0	0
"""
import sys
import MySQLdb
DB = "ugahash"
HOST = "localhost"
USER = "root"
PASSWD = open("/home/tyler/.dbpassword").read().strip()
db = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD, db=DB)
c = db.cursor()

def getsplicedtranscriptlen(uga):
    #get exon bounds from mysql
    spliced_length = 0

    c.execute("""SELECT hash_id, stop, start FROM hasher_sequence
        JOIN hasher_parentchildseqrelations ON hash_id = child_hash_id_id
        WHERE parent_hash_id_id = %s ;""", (uga, ))

    for x in c.fetchall():
        spliced_length += int(x[1]) - int(x[2])
        #print(x)
        assert int(x[1]) - int(x[2]) >= 0

    return spliced_length


for line in open(sys.argv[1]):

    spln = line.split()

    original_id_1 = spln[0]
    uga_1 = spln[1]

    original_id_2 = spln[5]
    uga_2 = spln[6]

    difference_start = int(spln[-2])
    difference_stop = int(spln[-1])

    """
    str(abs(start1 - start2) + abs(stop1 - stop2)),  # Total offset
    str(start1 - start2),  # Diff start
    str(stop1 - stop2)     # Diff stop
    """

    length_1 = getsplicedtranscriptlen(uga_1)
    length_2 = getsplicedtranscriptlen(uga_2)

    #print(length_1, length_2)

    # start is x, stop is y
    quadrand = "error"
    #print(difference_start, difference_stop)
    if   difference_start == 0 or difference_stop == 0:
        quadrand = "axis"
    elif difference_start > 0 and difference_stop > 0:
        quadrand = "quad1"
    elif difference_start < 0 and difference_stop < 0:
        quadrand = "quad3"
    elif difference_start < 0 and difference_stop > 0:
        quadrand = "quad2"
    elif difference_start > 0 and difference_stop < 0:
        quadrand = "quad4"

    print(
        "\t".join(
            [
                original_id_1,
                uga_1,
                str(length_1),
                original_id_2,
                uga_2,
                str(length_2),
                str(difference_start),
                str(difference_stop),
                quadrand,
                str(length_1 - length_2)
            ]
        )
    )
