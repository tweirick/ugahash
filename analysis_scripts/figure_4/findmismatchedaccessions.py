"""
Ensembl_Transcript_AC\tother_ids...
"""
from __future__ import print_function
import argparse
import MySQLdb

import sys
DB = "ugahash"
HOST = "localhost"
USER = "root"
PASSWD = open("/home/tyler/.dbpassword").read().strip()
db = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD, db=DB)
c = db.cursor()


trans_dict = {

'ribozyme': 'ribozyme',
'scaRNA':   'scaRNA',
'sRNA': 'sRNA',

'TEC':             'misc',
'known_ncrna':     'misc',

'protein_coding':          'protein_coding',
'IG_C_gene':               'protein_coding',
'IG_D_gene':               'protein_coding',
'IG gene':                 'protein_coding',
'IG_J_gene':               'protein_coding',
'IG_LV_gene':              'protein_coding',
'IGM gene':                'protein_coding',
'IG_V_gene':               'protein_coding',
'IGZ gene':                'protein_coding',
'nonsense mediated decay': 'protein_coding',
'nonsense_mediated_decay': 'protein_coding',
'nontranslating CDS':      'protein_coding',
'non stop decay':          'protein_coding',
'non_stop_decay':          'protein_coding',
'TR_C_gene':               'protein_coding',
'TR_D_gene':               'protein_coding',
'TR_J_gene':               'protein_coding',
'TR_V_gene':               'protein_coding',

'pseudogene':                         'pseudogene',
'polymorphic pseudogene':             'pseudogene',
'polymorphic_pseudogene':             'pseudogene',
'disrupted domain':                   'pseudogene',
'IG_C_pseudogene':                    'pseudogene',
'IG_J_pseudogene':                    'pseudogene',
'IG_pseudogene':                      'pseudogene',
'IG_V_pseudogene':                    'pseudogene',
'TR_V_pseudogene':                    'pseudogene',
'processed pseudogene':               'pseudogene',
'processed_pseudogene':               'pseudogene',
'transcribed_processed_pseudogene':   'pseudogene',
'transcribed processed pseudogene':   'pseudogene',
'transcribed unitary pseudogene':     'pseudogene',
'transcribed_unitary_pseudogene':     'pseudogene',
'unitary_pseudogene':                 'pseudogene',
'transcribed_unprocessed_pseudogene': 'pseudogene',
'translated_unprocessed_pseudogene':  'pseudogene',
'translated_processed_pseudogene':    'pseudogene',
'TR_J_pseudogene':                    'pseudogene',
'unprocessed_pseudogene':             'pseudogene',

'3prime_overlapping_ncrna': 'lncRNA',
'ambiguous orf':            'lncRNA',
'antisense':                'lncRNA',
'antisense RNA':            'lncRNA',
'lincRNA':                  'lncRNA',
'ncrna host':               'lncRNA',
'processed transcript':     'lncRNA',
'processed_transcript':     'lncRNA',
'sense intronic':           'lncRNA',
'sense_intronic':           'lncRNA',
'sense_overlapping':        'lncRNA',
'retained_intron':          'lncRNA',
'retained intron':          'lncRNA',
'non_coding':               'lncRNA',
'non coding':               'lncRNA',
'macro_lncRNA':               'lncRNA',

'miRNA':              'sncRNA',
'miRNA_pseudogene':   'sncRNA',
'miscRNA':            'sncRNA',
'misc_RNA':           'sncRNA',
'miscRNA pseudogene': 'sncRNA',
'Mt_rRNA':            'sncRNA',
'Mt_tRNA':            'sncRNA',
'rRNA':               'sncRNA',
'scRNA':              'sncRNA',
'snlRNA':             'sncRNA',
'snoRNA':             'sncRNA',
'snRNA':              'sncRNA',
'tRNA':               'sncRNA',
'tRNA_pseudogene':    'sncRNA'

}


parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
#dest='idmap',
#dest='gv',
parser.add_argument('--idmap', required=True,  help="A tsv file with Ensembl IDs mapped to other IDs")#nargs='+',
parser.add_argument('--gv',    required=True, help="Genome Version")
args = parser.parse_args()
file_name = args.idmap


found = 0
hash1_not_found_cnt = 0
hash2_not_found_cnt = 0
both_not_found = 0

#Ensmebl does not allow us to
ensembl_ac_dict = {}
for line in open(file_name):

    split_line = [x for x in line.strip().split("\t") if x != ""]
    #Skip entries without relations.
    if len(split_line) > 1:

        if split_line[0] not in ensembl_ac_dict:
            ensembl_ac_dict.update({split_line[0]: set()})

        ensembl_ac_dict[split_line[0]].update(split_line[1:])
        z = split_line[0]

#print(ensembl_ac_dict[z])
#exit()

file_obj = open(file_name+".calc_offset.tsv", 'w')
genome_version = args.gv

for el in ensembl_ac_dict:

    id1 = el
    c.execute("""SELECT DISTINCT hash_id_id FROM hasher_sequencesetsequencerelation WHERE original_id = %s AND source_version_id_id = "Ensembl_79" ;""",
              (id1, )) #genome_version,
              #(id1+"%", )) #genome_version,
    hash1 = set([x[0] for x in c.fetchall() if x[0][:-22].split("_")[-1] == genome_version])


    hash2 = set()
    for id2 in ensembl_ac_dict[el]:
        #Note addition of % to end of id2 search, to account for NCBI versioning  NC_010004.1 ->  NC_010004
        c.execute("""SELECT DISTINCT hash_id_id FROM hasher_sequencesetsequencerelation WHERE original_id = %s AND source_version_id_id = "NCBI_107" ;""",
              (id2,)) #genome_version,
              #(id2+".%",)) #genome_version,



        hash2 = hash2 | set([x[0] for x in c.fetchall() if x[0][:-22].split("_")[-1] == genome_version])

    if not hash1 & hash2 and hash1 != set([]) and hash2 != set([]):

        #We only need one Ensembl position.
        c.execute("""SELECT chromosome, start, stop FROM hasher_sequence WHERE hash_id = %s;""", (list(hash1)[0], ))
        bounds1 = c.fetchone()

        chromosome1 = bounds1[0]
        start1 = int(bounds1[1])
        stop1 = int(bounds1[2])

        #Select the closest hash in the case of multiple relations.
        total_dif = None
        chromosome2 = None
        start2 = None
        stop2 = None
        for hash2_el in hash2:

            c.execute("""SELECT chromosome, start, stop FROM hasher_sequence WHERE hash_id = %s;""", (list(hash2)[0], ))
            bounds2 = c.fetchone()

            tmp_chromosome2 = bounds1[0]
            tmp_start2 = int(bounds2[1])
            tmp_stop2 = int(bounds2[2])
            tmp_total_dif = abs(start1 - tmp_start2) + abs(stop1 - tmp_stop2)

            if total_dif is None or tmp_total_dif < total_dif:

                chromosome2 = tmp_chromosome2
                start2 = tmp_start2
                stop2 = tmp_stop2

        print(
            "\t".join([
                #trans_dict[spln[1]], spln[1],
                id1,
                list(hash1)[0],
                chromosome1,
                str(start1),
                str(stop1),
                id2,
                list(hash2)[0],
                chromosome2,
                str(start2),
                str(stop2),
                str(abs(start1 - start2) + abs(stop1 - stop2)),  # Total offset
                str(start1 - start2),  # Diff start
                str(stop1 - stop2)     # Diff stop
            ]), file=file_obj)

    else:

        if hash1 & hash2 and hash1 != set([]) and hash2 != set([]):
            found += 1
        if hash1 == set([]) and hash2 != set([]):
            hash1_not_found_cnt += 1
            print( "hash1_not_found_cnt"+"\t"+id1.strip())
        if hash2 == set([]) and hash1 != set([]):
            hash2_not_found_cnt += 1
            print("hash2_not_found_cnt"+"\t"+id2.strip())
        if hash1 == set([]) and hash2 == set([]):
            both_not_found += 1
            print("both_not_found_id1"+"\t"+id1.strip())
            print("both_not_found_id2"+"\t"+id2.strip())


print("found", found)
print("hash1_not_found_cnt", hash1_not_found_cnt)
print("hash2_not_found_cnt", hash2_not_found_cnt)
print("both_not_found", both_not_found)


"""
SELECT DISTINCT hash_id_id FROM hasher_sequencesetsequencerelation JOIN hasher_sequenceset ON
              source_version_id_id = source_version_id  WHERE original_id LIKE "NM_020423" AND assembly = "GRCh38";
"""
