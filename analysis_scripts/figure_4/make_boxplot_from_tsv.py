__author__ = 'tyler'

from pylab import *
import argparse
#matplotlib.rc('font', serif='Helvetica')

titles = []
data_list = []
ax = axes()

parser = argparse.ArgumentParser()
parser.add_argument('--infile', dest='infile', required=True, help="")
#parser.add_argument('--title' , dest='title' , required=True, help="")
parser.add_argument('--xlabel', dest='xlabel', required=True, help="")
parser.add_argument('--ylabel', dest='ylabel', required=True, help="")
args = parser.parse_args()

use_percent = False

#file_name = "/home/tyler/Desktop/VISTA_lengths.csv"
file_name = args.infile
xlabel = args.xlabel
ylabel = args.ylabel
delim = "\t"
#chart_title = args.title
for line in open(file_name):
    if titles == []:
        titles = line.strip("\n").split(delim)
        print titles
        for el in titles:
            data_list.append([])
    else:
        data_els = line.strip("\n").split(delim)
        #print data_els

        assert len(data_els) == len(titles), 'build error checking all rows must be the same length'

        for i in range(len(data_els)):

            if data_els[i] != '':
                if use_percent:
                    data_list[i].append( round(float(data_els[i])*100.0, 1) )
                else:
                    data_list[i].append( float(data_els[i] ))



# fake up some data
#spread= rand(50) * 100
#center = ones(25) * 50
#flier_high = rand(10) * 100 + 100
#flier_low = rand(10) * -100
#data =concatenate((spread, center, flier_high, flier_low), 0)

# fake up some more data
#spread= rand(50) * 100
#center = ones(25) * 40
#flier_high = rand(10) * 100 + 100
#flier_low = rand(10) * -100
#d2 = concatenate( (spread, center, flier_high, flier_low), 0 )
#data.shape = (-1, 1)
#d2.shape = (-1, 1)
#data = concatenate( (data, d2), 1 )
# Making a 2-D array only works if all the columns are the
# same length.  If they are not, then use a list instead.
# This is actually more efficient because boxplot converts
# a 2-D array into a list of vectors internally anyway.
#data = [data, d2, d2[::2,0]]
# multiple box plots on one figure
#figure()

print "25th_percentile", "Average", "75th_percentile", 'median', 'stddev'
for data_el in data_list:
    print np.percentile(data_el, 25), np.average(data_el), np.percentile(data_el, 75), np.median(data_el), np.std(data_el), len(data_el)

for el in titles:
    print el

#boxplot(data_list)
#xticks([1, 2, 3], titles)
ax.set_xticklabels(titles)
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)
#ax.title(chart_title)
#ax.set_aspect(0.1)
bp = boxplot(data_list, sym='', widths=0.5)
#title(chart_title)
hold(True)

setp(bp['boxes'],    color='black')
setp(bp['caps'],     color='black')
#setp(bp['caps'],     color='black')
setp(bp['whiskers'], color='black')
#setp(bp['whiskers'], color='black')
setp(bp['fliers'],   color='black')
#setp(bp['fliers'],   color='black')
setp(bp['medians'],  color='black')

show()
