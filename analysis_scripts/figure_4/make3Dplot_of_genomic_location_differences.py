import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import sys
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import matplotlib.colors as colors
from matplotlib.pyplot import cm
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.patches as mpatches
from pylab import *

matplotlib.rcParams.update({'font.size': 20})

fig = plt.figure()
ax = SubplotZero(fig, 111)
file_name = sys.argv[1]
chromosome_dict = {}

for line in open(file_name):
    spln = line.split()

    chromo = spln[2]

    x = int(spln[-1])
    y = int(spln[-2])

    if not "CHR_" in chromo:
        if not chromo in chromosome_dict:
            chromosome_dict.update({chromo: [[], []]})

        chromosome_dict[chromo][0].append(x)
        chromosome_dict[chromo][1].append(y)

#for el in sorted(chromosome_dict, key=chromosome_dict.get):
#    print(el, len(chromosome_dict[el][0]))

NUM_COLORS = len(chromosome_dict)
cmap = cm.rainbow(np.linspace(0, 1, NUM_COLORS))
handle_list = []
lablel_list = []
i = 0
total = 0

for el in sorted(chromosome_dict):
    x = chromosome_dict[el][0]
    y = chromosome_dict[el][1]
    total += len(x)
    ax.scatter(x, y, s=40, c=cmap[i], marker='o')
    handle_list.append(mpatches.Patch(color=cmap[i], label=el))
    lablel_list.append(el)
    i += 1

fig.legend(handle_list, lablel_list, 'upper right')
#legend = ax.legend(loc='upper center', shadow=True)
for direction in ["xzero", "yzero"]:
    #ax.axis[direction].set_axisline_style("--")
    ax.axis[direction].set_visible(True)
    ax.axis[direction].label.set_visible(False)
#for direction in ["left", "right", "bottom", "top"]:
#    ax.axis[direction].set_visible(False)
#plt.axhline(0, color='black')
#plt.axvline(0, color='black')
#make_xaxis(ax, 0, offset=0.1, **props)
#exit()
#draw straight line
#plt.plot([0, 0], [-4,4], lw=3, 'w')
print(total)
ax.set_xlabel('Difference between Ensembl and NCBI transcript start position.')
ax.set_ylabel('Difference between Ensembl and NCBI transcript stop position.')
fig.add_subplot(ax)
plt.show()