import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import sys
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import matplotlib.colors as colors
from matplotlib.pyplot import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.patches as mpatches
from pylab import *
import re

def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key=alphanum_key)

matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure(figsize=(14.0, 14.0))
ax = SubplotZero(fig, 111)
file_name = sys.argv[1]
chromosome_dict = {}

x, y = [], []
for line in open(file_name):
    spln = line.split()

    chromo = spln[2]

    x1 = int(spln[-1])
    y1 = int(spln[-2])

    x.append(x1)
    y.append(y1)

    if not "CHR_" in chromo:
        if not chromo in chromosome_dict:
            chromosome_dict.update({chromo: [[], []]})

        chromosome_dict[chromo][0].append(x1)
        chromosome_dict[chromo][1].append(y1)

x = np.array(x)
y = np.array(y)

NUM_COLORS = len(chromosome_dict)
cmap = cm.rainbow(np.linspace(0, 1, NUM_COLORS))
handle_list = []
lablel_list = []
i = 0
total = 0
x_chr_bin = []
y_chr_bin = []
for el in natural_sort(chromosome_dict):

    x_chr_bin.append(chromosome_dict[el][0])
    y_chr_bin.append(chromosome_dict[el][1])

    x1 = chromosome_dict[el][0]
    y1 = chromosome_dict[el][1]

    ax.scatter(x1, y1, s=40, c=cmap[i], edgecolor="none", marker='.')
    handle_list.append(mpatches.Patch(color=cmap[i], label=el))
    lablel_list.append(el)
    i += 1

x_max = np.max(np.fabs(x))
y_max = np.max(np.fabs(y))
xymax = np.max([x_max, y_max])
binwidth = int(ceil(xymax/50.0))
lim = int(xymax + 1)

bins = list(np.arange(-lim, lim + binwidth, binwidth))
bins_len = len(bins) - 1
bins_front = bins[0:bins_len/2]
bins_back = bins[bins_len/2 + 1:len(bins)]

bins = np.append(
    bins_front,
    [-round(binwidth/3.0, 2), round(binwidth/3.0, 2)]
)

bins = np.append(
    bins,
    bins_back
)

print(bins)


divider = make_axes_locatable(ax)
axHistx = divider.append_axes("top",   1.2, pad=0.1, sharex=ax)
axHisty = divider.append_axes("right", 1.2, pad=0.1, sharey=ax)
# make some labels invisible
plt.setp(axHistx.get_xticklabels() + axHisty.get_yticklabels(), visible=False)
# now determine nice limits by hand:

axHistx.hist(
    x_chr_bin,
    bins=bins,
    color=cmap,
    histtype='bar',
    stacked=True,
    edgecolor="none"
)

axHisty.hist(
    y_chr_bin,
    bins=bins,
    color=cmap,
    orientation='horizontal',
    histtype='bar', stacked=True,
    edgecolor="none"
)


axHistx.axis["bottom"].major_ticklabels.set_visible(False)
for tl in axHistx.get_xticklabels():
    tl.set_visible(False)
axHistx.set_yticks([0, len(x)/2, len(x)])

axHisty.axis["left"].major_ticklabels.set_visible(False)
for tl in axHisty.get_yticklabels():
    tl.set_visible(False)
axHisty.set_xticks([0, len(x)/2, len(x)])

ax.set_xticks([0], minor=True)
ax.set_yticks([0], minor=True)
ax.grid(which="minor")

fig.legend(handle_list, lablel_list, bbox_to_anchor=(1.00, 0.90)) #'upper right',

ax.set_xlabel('Difference between Ensembl and NCBI transcript start position.')
ax.set_ylabel('Difference between Ensembl and NCBI transcript stop position.')
fig.add_subplot(ax)
#plt.show()
plt.savefig(sys.argv[1].split("/")[-1]+'_foo.png')
plt.savefig(sys.argv[1].split("/")[-1]+'_foo.svg')