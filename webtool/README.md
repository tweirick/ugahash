### Installing and Developing Django.

Starting out with the Django framework can have a steep learning curve. We recomend using the documentation on [Django's website](https://www.djangoproject.com/) for instructions regarding how to install the Django framework.

### Running UGAHash.

##### Running a local instance.
After successfully installing the Django libraries the UGAHash webtool can be tested by executing the following command from the command line in the webtools base directory.

```bash
python manage.py runserver
```
Then simply going to 127.0.0.1:8000 in your web browser.

> **Note:**  This will use the sample sqlite database provided with the webtool. If you would like to change the database software this can be change in ugahash/settings.py. See the documentation on [Django's website](https://www.djangoproject.com/) for more details.

##### Running a public instance.

Seeting up a public instance of Django based is more complicated. See the documentation on [Django's website](https://www.djangoproject.com/) for more details.


### Adding data.

An sql dump of the original webtool can be on the webtools [help page](http://ugahash.uni-frankfurt.de/help/)

While the original UGAHash database only includes human data, UGAHash was designed to hold multiple species. If you wish to include additional species or databases the helper script "make_insert_tsvs.py" is included. This program takes a [JSON formatted GFF file] and will generate the tsv files for each table needed in the UGAHash webtool.

Use the following command for the programs help options.
```bash
python make_insert_tsvs.py --help
```

Here is an example from a recent addition to the web tool's database.
```bash
python make_insert_tsvs.py \
--json_file Homo_sapiens.GRCh38.83.gtf.json \
--source Ensembl \
--source_version 83 \
--source_base_url "http://dec2015.archive.ensembl.org/Multi/Search/Results?q=%s;site=ensembl" \
--assembly GRCh38 \
--tax_id 9606 >> setset.tsv
```

This created the files:
- Ensembl_83_9606_parent_child.txt
- Ensembl_83_9606_sequence_set.txt
- Ensembl_83_9606_sequences.txt
- setset.tsv

To add these to a database, for example mysql: 

```bash
mysql -u root -p --local-infile
```

```mysql
LOAD DATA LOCAL INFILE "setset.tsv" IGNORE INTO TABLE hasher_sequenceset;

LOAD DATA LOCAL INFILE "Ensembl_83_9606_sequences.txt" IGNORE INTO TABLE hasher_sequence;

LOAD DATA LOCAL INFILE "Ensembl_83_9606_parent_child.txt"
IGNORE INTO TABLE hasher_parentchildseqrelations;

LOAD DATA LOCAL INFILE "Ensembl_83_9606_sequence_set.txt"
IGNORE INTO TABLE hasher_sequencesetsequencerelation;
```
