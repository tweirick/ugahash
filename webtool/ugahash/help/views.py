from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from hasher.models import Sequence, SequenceSet, ParentChildSeqRelations, SequenceSetSequenceRelation


def index(request):
    """
    Perform basic Queries such as databases included in UGAHash.
    :param request:
    :return:
    """


    #tax_id, assembly, chromosome, strand, start, stop = Sequence.objects.filter(hash_id=hash_id).values_list(
    #    "tax_id", "assembly", "chromosome", "strand", "start", "stop")[0]
    distinct_sources = SequenceSet.objects.order_by().values_list('source').distinct()

    databases_and_versions = []
    for source_el in sorted(distinct_sources):

        s_el = source_el[0]

        versions = SequenceSet.objects.order_by().filter(source=s_el).values_list('version').distinct()

        databases_and_versions.append([s_el, [v[0] for v in versions]])

    #databases_and_versions = [
    #    ["ENSEMBL", ["78", "79"]],
    #    ["NONCODE", ["4"]]
    #]

    context = RequestContext(request, {"databases_and_versions": databases_and_versions})
    template = loader.get_template('help/help.html')
    return HttpResponse(template.render(context))
