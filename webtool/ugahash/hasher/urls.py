from django.conf.urls import url
from . import views

urlpatterns = [
    #url(r'^/hashmapped/$', views.hashmapped, name='hashmapped'),
    #url(r'^sequence/', views.sequence, name='sequence'),
    url(r'^sequence/(?P<hash_id>.*)$', views.sequence, name='sequence'),
    url(r'^json/(?P<hash_id>.*)$', views.externalids, name='externalids'),
    url(r'^$', views.index, name='index'),
]
