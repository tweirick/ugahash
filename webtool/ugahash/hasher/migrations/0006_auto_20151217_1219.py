# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hasher', '0005_sequence_tax_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='seqsetrels',
            field=models.ManyToManyField(to='hasher.SequenceSet', through='hasher.SequenceSetSequenceRelation'),
        ),
        migrations.AlterField(
            model_name='sequencesetsequencerelation',
            name='id',
            field=models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
        ),
    ]
