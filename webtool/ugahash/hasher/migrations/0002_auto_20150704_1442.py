# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hasher', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SequenceSetSequenceRelation',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('original_id', models.CharField(max_length=80)),
                ('hash_id', models.ForeignKey(to='hasher.Sequence')),
                ('source_version_id', models.ForeignKey(to='hasher.SequenceSet')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='sequencesetsequencerelation',
            unique_together=set([('source_version_id', 'hash_id')]),
        ),
    ]
