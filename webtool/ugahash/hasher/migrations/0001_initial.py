# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sequence',
            fields=[
                ('hash_id', models.CharField(max_length=80, serialize=False, primary_key=True)),
                ('assembly', models.CharField(max_length=80)),
                ('chromosome', models.CharField(max_length=80)),
                ('strand', models.CharField(max_length=1)),
                ('start', models.IntegerField()),
                ('stop', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='SequenceSet',
            fields=[
                ('source_version_id', models.CharField(max_length=80, serialize=False, primary_key=True)),
                ('source', models.CharField(max_length=80)),
                ('version', models.CharField(max_length=80)),
                ('tax_id', models.CharField(max_length=80)),
                ('assembly', models.CharField(max_length=80)),
                ('link', models.CharField(max_length=400)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='sequenceset',
            unique_together=set([('source', 'version', 'tax_id')]),
        ),
        migrations.AlterUniqueTogether(
            name='sequence',
            unique_together=set([('hash_id', 'assembly', 'chromosome', 'strand', 'start', 'stop')]),
        ),
    ]
