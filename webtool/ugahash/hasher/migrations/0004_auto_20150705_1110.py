# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hasher', '0003_auto_20150704_1450'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='seq_type',
            field=models.CharField(default='exon', max_length=80),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='sequence',
            unique_together=set([]),
        ),
    ]
