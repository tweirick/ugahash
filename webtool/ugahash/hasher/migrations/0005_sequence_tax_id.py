# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hasher', '0004_auto_20150705_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='tax_id',
            field=models.CharField(default='9606', max_length=80),
            preserve_default=False,
        ),
    ]
