# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hasher', '0002_auto_20150704_1442'),
    ]

    operations = [
        migrations.CreateModel(
            name='ParentChildSeqRelations',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('parent_type', models.CharField(max_length=80)),
                ('child_type', models.CharField(max_length=80)),
                ('rank', models.IntegerField()),
                ('child_hash_id', models.ForeignKey(related_name='child_hash_id', to='hasher.Sequence')),
                ('parent_hash_id', models.ForeignKey(related_name='parent_hash_id', to='hasher.Sequence')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='parentchildseqrelations',
            unique_together=set([('parent_hash_id', 'parent_type', 'child_hash_id', 'child_type', 'rank')]),
        ),
    ]
