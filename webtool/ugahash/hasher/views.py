from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import RequestContext, loader
from .forms import HashMapForm
from ugahash.resources import HashMapHasher
from ugahash.resources.CrossMap import map_coordinates
from ugahash import settings
import csv
from .models import Sequence, SequenceSet, ParentChildSeqRelations, SequenceSetSequenceRelation
import json


def generategtfs(gtf_lines, species, assembly):
    """ Yields data parsed from GTF/GFF files.

    :param gtf_lines: A set of GTF/GFF formatted lines.
    :param species: A tax id the gtf_lines are from.
    :param assembly: The assembly version the gtf_lines are from.
    :return: Yields parsed GTF/GFF data.
    """
    exon_bound_list = []
    prev_seq_type = None
    prev_chromosome = None
    prev_start = None
    prev_stop = None
    prev_frame = None
    prev_strand = None
    prev_something = None

    for line in gtf_lines:
        spln = line.split()

        chromosome = spln[0]
        source  = spln[1]
        seq_type = spln[2]
        start = spln[3]
        stop = spln[4]
        frame = spln[5]
        strand = spln[6]
        something = spln[7]

        if seq_type == "gene":

            if not exon_bound_list == []:
                exon_bound_list = sorted(exon_bound_list)
                hashed_ac = HashMapHasher.UGAHash(
                    prev_seq_type[0].upper(),
                    assembly,
                    prev_chromosome,
                    prev_strand,
                    exon_bound_list
                )

                yield {
                       "seq_type": prev_seq_type,
                       "chromosome": prev_chromosome,
                       "strand": strand,
                       "exon_bound_list": exon_bound_list
                       }

                exon_bound_list = []

            yield {
                   "seq_type": seq_type,
                   "chromosome": chromosome,
                   "strand": strand,
                   "exon_bound_list": ((start, stop),)
            }

        elif seq_type == "transcript":

            if not exon_bound_list == []:
                exon_bound_list = sorted(exon_bound_list)

                yield {
                    # "hashed_ac": hashed_ac,
                    "seq_type": prev_seq_type,
                    "chromosome": prev_chromosome,
                    "strand": strand,
                    "exon_bound_list": exon_bound_list,
                }
                exon_bound_list = []

            prev_seq_type = seq_type
            prev_chromosome = chromosome
            prev_start = start
            prev_stop = stop
            prev_strand = strand
            prev_something = something

        elif seq_type == "exon":
            exon_bound_list.append((int(start), int(stop)))

            yield {
                   "seq_type": seq_type,
                   "chromosome": chromosome,
                   "strand": strand,
                   "exon_bound_list": ((start, stop),)
            }

        else:
            yield {
                   "seq_type": seq_type,
                   "chromosome": chromosome,
                   "strand": strand,
                   "exon_bound_list": ((start, stop),)
            }

    exon_bound_list = sorted(exon_bound_list)

    if exon_bound_list:
        yield {
            "seq_type": prev_seq_type,
            "chromosome": prev_chromosome,
            "strand": strand,
            "exon_bound_list": exon_bound_list

        }


def hashactotype(hash_id):

    if "UG_" == hash_id[:3]:
        return "gene"
    elif "UT_" == hash_id[:3]:
        return "transcript"
    elif "UE_" == hash_id[:3]:
        return "exon"
    else:
        return False


def getsequencetype(hash_id):
    if hash_id[1] == "G":
        return 'gene'
    elif hash_id[1] == "T":
        return 'transcript'
    elif hash_id[1] == "E":
        return 'exon'
    else:
        assert False


def getrevervedseqtype(sequence_type):
    if sequence_type.lower() == "gene":
        return "G"
    elif sequence_type.lower() == "transcript":
        return "T"
    elif sequence_type.lower() == "exon":
        return "E"
    return sequence_type


def resolvefileortext(input_text, file_input_key, request):
    """
    :param input_text:
    :param file_input_key:
    :param request:
    :return:
    """
    error_message = ""
    if input_text == "":
        if file_input_key in request.FILES:
            input_text = request.FILES[file_input_key].read()
        else:
            error_message = "Must submit file or text. No data submitted."
    else:
        if file_input_key in request.FILES:
            error_message = "Must submit file or text. Cannot accept both."
    return input_text, error_message


def existencestate(hashed_ac, source, tax_id, assembly, chromosome, strand, start, stop, seq_type):

    match_exists = SequenceSet.objects.filter(
                sequencesetsequencerelation__hash_id=hashed_ac,
                source=source).exists()

    if not match_exists:
        # Check for overlap.
        start, stop = int(start), int(stop)
        total_len = int((stop - start)*0.25)

        for map_tree_assembly in settings.CHAIN_FILE_DICT[tax_id]:

            use_assembly = assembly
            use_start = start
            use_stop = stop

            if map_tree_assembly != assembly:

                mapped_coordinates = map_coordinates(
                    settings.CHAIN_FILE_DICT[tax_id][assembly][map_tree_assembly],
                    'chr'+chromosome,
                    int(start),
                    int(stop),
                    strand
                )

                try:
                    use_start, use_stop = sorted([mapped_coordinates[-1][1], mapped_coordinates[-1][2]])
                except (ValueError, IndexError):
                    use_start, use_stop = None, None

                use_assembly = map_tree_assembly

            if use_start and use_stop:

                overlap_exists = Sequence.objects.filter(
                    tax_id=tax_id,
                    assembly=use_assembly,
                    chromosome=chromosome,
                    strand=strand,
                    seqsetrels__source=source,
                    seq_type=seq_type,
                    start__range=[start - total_len, start + total_len],
                    stop__range=[stop - total_len, stop + total_len]
                ).exclude(hash_id=hashed_ac).exists()

                if overlap_exists:
                    # Add maybe.
                    match_exists = "overlap"

    return str(match_exists)


def getversionsdict():

    versions_sources = SequenceSet.objects.values_list('source', 'source_version_id').distinct()
    source_dict = {}
    for tup_el in versions_sources:

        source, version = tup_el
        if source not in source_dict:
            source_dict[source] = []

        source_dict[source].append(version)

    return source_dict


def generateboundlist(exon_bound_list, hashed_ac, tax_id, seq_type, assembly, ugahash_args):

    bound_list = []
    prev_start, prev_stop = None, None
    for start, stop in exon_bound_list:
        tmp_start, tmp_stop = sorted([int(start), int(stop)])
        assert ((prev_start is None or prev_start < tmp_start) and (prev_stop is None or prev_stop < tmp_stop))
        bound_list.append(str(tmp_start) + "-" + str(tmp_stop))
        prev_start, prev_stop = tmp_start, tmp_stop

    """%(hash_ac)s?tax_id=%(tax_id)s&sequence_type=%(seq_type)s&assembly=%(assembly)s&chromosome=%(chromosome)s"""

    hash_link = (hashed_ac+"?" +
                 "tax_id=" + tax_id +
                 "&sequence_type=" + seq_type +
                 "&assembly=" + assembly +
                 "&chromosome=" + ugahash_args["chromosome"] +
                 "&strand=" + ugahash_args["strand"].replace("+", "%2B") +
                 "&bounds=" + ":".join(bound_list))

    return hash_link


def index(request):
    """

    :param request:
    :return:
    """

    if request.method == 'GET' and request.GET != {}:

        form = HashMapForm(request.GET, request.FILES)

        if form.is_valid():
            # Get text input.
            text_input, error_message = resolvefileortext(form.cleaned_data['text_input'], 'file_input', request)
            # Get species and assembly.
            tax_id, assembly = form.cleaned_data['species_assembly'].split("-")
            # Get list of distinct sources.
            distinct_sources = sorted(SequenceSet.objects.values_list('source', flat=True).distinct())

            source_dict = sorted(getversionsdict())

            # Iterate over the GFF annotations.
            hashed_seqs = []
            tsv_hashed_seqs = []
            json_hashed_seqs = []
            try:
                for ugahash_args in generategtfs(text_input.strip().split("\n"), tax_id, assembly):

                    seq_type = ugahash_args["seq_type"]
                    chromosome = ugahash_args["chromosome"].strip("chr")
                    strand = ugahash_args["strand"]
                    exon_bound_list = ugahash_args["exon_bound_list"]
                    start = exon_bound_list[0][0]
                    stop = exon_bound_list[-1][-1]

                    # Generate UGA
                    hashed_ac = HashMapHasher.UGAHash(
                        getrevervedseqtype(seq_type),  # Return the tag for the type of feature being hashed.
                        assembly,
                        chromosome,
                        strand,
                        exon_bound_list
                    )

                    # Add basic information for output page.
                    tmp_hashed_seqs = {
                        "hash_id": hashed_ac,
                        "hash_id_link": generateboundlist(
                            exon_bound_list, hashed_ac, tax_id, seq_type, assembly, ugahash_args),
                        "seq_type": seq_type,
                        "chromosome": chromosome,
                        "strand": strand,
                        "start": start,
                        "stop": stop,
                        "existences": []
                    }

                    # Check for existence in other databases.
                    for source_el in source_dict:

                        tmp_existence = existencestate(
                            hashed_ac,
                            source_el,
                            tax_id,
                            assembly,
                            chromosome,
                            strand,
                            start,
                            stop,
                            seq_type
                        )
                        tmp_hashed_seqs["existences"].append({
                            "source": source_el,
                            "existence_state": tmp_existence
                        })

                    hashed_seqs.append(tmp_hashed_seqs)

                template = loader.get_template('hasher/hashmapped.html')

                context_data = {
                    "tax_id": tax_id,
                    "assembly": assembly,
                    "hashed_seqs": hashed_seqs,
                    "distinct_sources": distinct_sources,
                }

                context = RequestContext(request, context_data)

                if "format" in request.GET and request.GET["format"] == "json":
                    output = json.dumps(context_data, indent=4)
                    return HttpResponse(output, content_type="application/json")

                elif "format" in request.GET and request.GET["format"] == "tsv":

                    response = HttpResponse(content_type='text/plain')
                    writer = csv.writer(response, delimiter='\t')
                    writer.writerow(
                        ["#"+"hash_id", "seq_type", "chromosome", "strand", "start", "stop"] + distinct_sources)

                    for uga_key in hashed_seqs:
                        writer.writerow([
                               uga_key["hash_id"],
                               uga_key["seq_type"],
                               uga_key["chromosome"],
                               uga_key["strand"],
                               uga_key["start"],
                               uga_key["stop"],
                            ] + [el['existence_state'] for el in uga_key["existences"]]
                        )
                    return response
                else:
                    context.update({
                        "json_link": request.build_absolute_uri() + "&format=json",
                        "tsv_link": request.build_absolute_uri() + "&format=tsv"
                    })

                    return HttpResponse(template.render(context))

            except:
                template = loader.get_template('hasher/index.html')
                context = RequestContext(request, {
                    "form": form,
                    "error_message": "Error parsing GFF data. Please check the format of your input."})

        else:
            # Handle errors.
            template = loader.get_template('hasher/index.html')
            context = RequestContext(request, {"form": form})
    else:
        form = HashMapForm()
        template = loader.get_template('hasher/index.html')
        context = RequestContext(request, {"form": form})

    return HttpResponse(template.render(context))


def findrelatedsequences(
        hash_id, tax_id, sequence_type, chromosome, assembly, strand, mapped_start, mapped_stop, x=0.25):
    """ Use to find sequences related to the queried sequence i.e. the start and stop positions falling within
    X percent of the start and stop positions. I originally wanted to use overlap, but could not figure out
    a way to find these efficiently.

    :param hash_id:
    :param tax_id:
    :param sequence_type:
    :param chromosome:
    :param assembly:
    :param strand:
    :param mapped_start:
    :param mapped_stop:
    :return:
    """

    grouped_data_struct = {}
    mapped_start, mapped_stop = int(mapped_start), int(mapped_stop)
    offset = int((mapped_stop - mapped_start) * x)

    sequence_in_other_assembly = Sequence.objects.filter(
        tax_id=tax_id,
        assembly=assembly,
        chromosome=chromosome,
        strand=strand,
        seq_type=sequence_type,
        start__range=[mapped_start - offset, mapped_start + offset],
        stop__range=[mapped_stop - offset, mapped_stop + offset]
    ).exclude(
        hash_id=hash_id
    ).values_list(
        'hash_id', 'assembly', 'chromosome', 'strand',
        'start', 'stop', 'sequencesetsequencerelation__original_id',
        'sequencesetsequencerelation__source_version_id'
    )

    for el in sequence_in_other_assembly:

        tmp_hash, tmp_assembly, tmp_chr, tmp_strand, tmp_start, tmp_stop, tmp_original_id, tmp_version,  = el

        if tmp_hash not in grouped_data_struct:
            grouped_data_struct.update(
                {
                    tmp_hash: {
                        "assembly":   tmp_assembly,
                        "chromosome": tmp_chr,
                        "strand":     tmp_strand,
                        "start":      tmp_start,
                        "stop":       tmp_stop,
                        "origin_data": []
                    }
                }
            )

        grouped_data_struct[tmp_hash]["origin_data"].append(
            {
                "version": tmp_version,
                "original_id": tmp_original_id,
            }
        )

    return grouped_data_struct


def externalids(request, hash_id):
    # Links to DBs
    sources_containing_hash = SequenceSet.objects.filter(
        sequencesetsequencerelation__hash_id=hash_id).values_list('source', 'version', 'link',
            'sequencesetsequencerelation__original_id').order_by('source', 'version')

    links_to_dbs = {}
    for source, version, link, original_id in reversed(sources_containing_hash):
        print(source, version, link, original_id)
        try:
            processed_link = link % original_id
        except TypeError:
            processed_link = "#"

        if original_id not in links_to_dbs:
            links_to_dbs.update(
                {
                    original_id: {
                        "source": "",
                        "version": "",
                        "link": ""
                    }
                }
            )

            links_to_dbs[original_id]["source"] = source
            links_to_dbs[original_id]["version"] = version
            links_to_dbs[original_id]["link"] = processed_link

    return JsonResponse(links_to_dbs)


def sequence(request, hash_id):
    """
    If posted specific database show only that else show all versions for give sequence.
    If single sequence given should show in sequence view.

    :param request:
    :param hash_id:
    :return:
    """
    sequence_details = []
    links_list = []
    transcript_list = []
    loc_search_list = []
    versions = []

    source_choice_exists = False
    TEMPLATE = loader.get_template('hasher/sequence_page.html')
    sequence_type = getsequencetype(hash_id)
    sources_containing_hash = []
    sequence_type = False
    source = None
    tax_id = False
    assembly = False
    chromosome = False
    bounds = False
    strand = False
    links_to_dbs = []
    parents = []
    children = []
    if request.method == 'GET':

        if "SOURCE" in request.GET:
            source = request.GET["SOURCE"]

        if Sequence.objects.filter(hash_id=hash_id).exists():

            tax_id, assembly, chromosome, strand, start, stop = Sequence.objects.filter(
                hash_id=hash_id).values_list("tax_id", "assembly", "chromosome", "strand", "start", "stop")[0]

            start, stop = sorted((start, stop))
            bounds = ((start, stop),)

            # Links to DBs
            sources_containing_hash = SequenceSet.objects.filter(
                sequencesetsequencerelation__hash_id=hash_id).values_list('source', 'version', 'link',
                    'sequencesetsequencerelation__original_id').order_by('source', 'version')

            links_to_dbs = []
            for source, version, link, original_id in sources_containing_hash:
                try:
                    links_to_dbs.append((source, version, original_id, link % original_id ))
                except TypeError:
                    links_to_dbs.append((source, version, original_id, link))
            # Parents
            parents = ParentChildSeqRelations.objects.values_list(
                'parent_hash_id__hash_id',
                'parent_hash_id__assembly',
                'parent_hash_id__seq_type',
                'parent_hash_id__chromosome',
                'parent_hash_id__strand',
                'parent_hash_id__start',
                'parent_hash_id__stop',
            ).filter(child_hash_id=hash_id).distinct()
            # Children
            children = ParentChildSeqRelations.objects.values_list(
                'child_hash_id__hash_id',
                'child_hash_id__assembly',
                'child_hash_id__seq_type',
                'child_hash_id__chromosome',
                'child_hash_id__strand',
                'child_hash_id__start',
                'child_hash_id__stop',
            ).filter(parent_hash_id=hash_id).order_by('rank')
            # Not sure why .distinct() isn't working in some instances.
            children = list(set(children))

        else:
            if "tax_id" in request.GET:
                tax_id = request.GET["tax_id"]
            if "assembly" in request.GET:
                assembly = request.GET["assembly"]
            if "chromosome" in request.GET:
                chromosome = request.GET["chromosome"]
            if "strand" in request.GET:
                strand = request.GET["strand"]
            if "bounds" in request.GET:
                bounds = []
                for el in request.GET["bounds"].split(":"):
                    start, stop = el.split("-")
                    bounds = (sorted((start, stop)),)

    # Overlapping in current assembly
    # for other assemblies of same organisms convert with liftover find related
    # Get other all unique human assemblies

    relations_between_assemblies = []
    sequence_type = hashactotype(hash_id)
    grouped_data_struct = {}

    # Skip if data not included.
    if tax_id in settings.CHAIN_FILE_DICT and assembly in settings.CHAIN_FILE_DICT[tax_id]:

        for map_tree_assembly in settings.CHAIN_FILE_DICT[tax_id]:
            use_assembly = assembly
            use_start = start
            use_stop = stop
            if map_tree_assembly != assembly:
                mapped_coordinates = map_coordinates(
                    settings.CHAIN_FILE_DICT[tax_id][assembly][map_tree_assembly],
                    'chr'+chromosome,
                    int(start),
                    int(stop),
                    strand
                )
                try:
                    use_start, use_stop = sorted([mapped_coordinates[-1][1], mapped_coordinates[-1][2]])
                except IndexError:
                    use_start, use_stop = None, None

                use_assembly = map_tree_assembly

            if use_start and use_stop:
                tmp_related_seqs = findrelatedsequences(
                        hash_id,
                        tax_id,
                        sequence_type,
                        chromosome,
                        use_assembly,
                        strand,
                        use_start,
                        use_stop
                    )
                grouped_data_struct.update(tmp_related_seqs)

    if not parents:
        parents = []
    if not children:
        children = []
    if not transcript_list:
        transcript_list = []
    if not loc_search_list:
        loc_search_list = []
    if not relations_between_assemblies:
        relations_between_assemblies = []
    if not grouped_data_struct:
        grouped_data_struct = []

    context = dict({
        "job_id": hash_id,
        "tax_id": tax_id,
        "sequence_type": sequence_type,
        "assembly": assembly,
        "chromosome": chromosome,
        "start": int(start),
        "stop": int(stop),
        "strand": strand,
        "seqset_obj": sequence_details,
        "versions": versions,
        "links_dict": list(links_list),
        "parents": list(parents),
        "children": list(children),
        "transcript_list": list(transcript_list),
        "loc_search_list": list(loc_search_list),
        "links_to_DBs": list(links_to_dbs),
        "relations_between_assemblies": list(relations_between_assemblies),
        "grouped_data_struct": grouped_data_struct
    })

    if "format" in request.GET and request.GET["format"] == "json":
        return JsonResponse(context)
    else:
        context.update({"json_link": request.build_absolute_uri() + "?&format=json"})
        return HttpResponse(TEMPLATE.render(context))
