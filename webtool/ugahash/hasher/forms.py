from django import forms
from .models import SequenceSet

#Generate the list of available species and assemblies from the database.
seq_set_obj = SequenceSet.objects.values_list('tax_id', 'assembly').distinct().order_by('tax_id', 'assembly')
SPECIES_ASSEMBLY_CHOICES = [(tax_id+'-'+assembly, tax_id+' - '+assembly) for tax_id, assembly in reversed(seq_set_obj)]
sample_file_path = "hasher/sample_input.gtf"


class HashMapForm(forms.Form):

    text_input = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={'placeholder': open('hasher/directions.html').read()})
    )

    species_assembly = forms.ChoiceField(
        required=True,
        initial=SPECIES_ASSEMBLY_CHOICES[0][0],
        choices=SPECIES_ASSEMBLY_CHOICES,
    )



