from django.db import models
"""
source_base_url = models.CharField(max_length=400) #http://www.noncode.org/show_gene.php?id=NONHSAG000001
gene_url = models.CharField(max_length=400) #http://www.noncode.org/show_gene.php?id=NONHSAG000001
transcript_url = models.CharField(max_length=400) #http://www.noncode.org/show_gene.php?id=NONHSAG000001
exon_url = models.CharField(max_length=400) #http://www.noncode.org/show_gene.php?id=NONHSAG000001
"""


class SequenceSet(models.Model):
    source_version_id = models.CharField(max_length=80, primary_key=True)
    source = models.CharField(max_length=80)
    version = models.CharField(max_length=80)
    tax_id = models.CharField(max_length=80)
    assembly = models.CharField(max_length=80)
    link = models.CharField(max_length=400)

    class Meta:
        unique_together = ('source', 'version', 'tax_id', )


class Sequence(models.Model):
    """
    ForeignKey.on_delete
    When an object referenced by a ForeignKey is deleted, Django by default emulates the behavior of the SQL
    constraint ON DELETE CASCADE and also deletes the object containing the ForeignKey. This behavior can be
    overridden by specifying the on_delete argument. For example, if you have a nullable ForeignKey
    and you want it to be set null when the referenced object is deleted:
    """
    #These four key feild together should be unique
    hash_id = models.CharField(max_length=80, primary_key=True)
    tax_id = models.CharField(max_length=80)
    seq_type = models.CharField(max_length=80)
    assembly = models.CharField(max_length=80)
    chromosome = models.CharField(max_length=80)
    strand = models.CharField(max_length=1) #(+|-|.)
    start = models.IntegerField()  # 3063334
    stop = models.IntegerField()  # 3064403

    seqsetrels = models.ManyToManyField(SequenceSet,
                                        through='SequenceSetSequenceRelation',
                                        through_fields=('hash_id', 'source_version_id',))

    #class Meta:
    #    unique_together = ('hash_id', 'assembly', 'chromosome', 'strand', 'start', 'stop')
    #def __unicode__(self):
    #    return unicode("\t".join([self.original_id, self.chromosome, str(self.start)]))


class SequenceSetSequenceRelation(models.Model):
    """
    ForeignKey.on_delete
    When an object referenced by a ForeignKey is deleted, Django by default emulates the behavior of the SQL
    constraint ON DELETE CASCADE and also deletes the object containing the ForeignKey. This behavior can be
    overridden by specifying the on_delete argument. For example, if you have a nullable ForeignKey
    and you want it to be set null when the referenced object is deleted:
    """
    #These four key feild together should be unique
    #id = models.AutoField(primary_key=True)
    source_version_id = models.ForeignKey(SequenceSet, on_delete=models.CASCADE)
    hash_id = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    original_id = models.CharField(max_length=80)

    class Meta:
        unique_together = ('source_version_id', 'hash_id')


class ParentChildSeqRelations(models.Model):
    """
    ForeignKey.on_delete
    When an object referenced by a ForeignKey is deleted, Django by default emulates the behavior of the SQL
    constraint ON DELETE CASCADE and also deletes the object containing the ForeignKey. This behavior can be
    overridden by specifying the on_delete argument. For example, if you have a nullable ForeignKey
    and you want it to be set null when the referenced object is deleted:
    """
    #These four key feild together should be unique
    id = models.AutoField(primary_key=True)
    parent_hash_id = models.ForeignKey(Sequence, related_name='parent_hash_id')  #models.CharField(max_length=80)
    parent_type = models.CharField(max_length=80)
    child_hash_id = models.ForeignKey(Sequence, related_name='child_hash_id')
    child_type = models.CharField(max_length=80)
    rank = models.IntegerField()

    class Meta:
        unique_together = ('parent_hash_id', 'parent_type', 'child_hash_id', 'child_type', 'rank')




