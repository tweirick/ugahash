class Parse:

    class Feature:

        def __init__(self, feature_line):
            """
            :param feature_line:
            :return:
            """
            spln = feature_line.strip().split()
            self.chromosome = spln[0].strip("chr")
            self.start = spln[1]
            self.end = spln[2]
            self.strand = "."
            if len(spln) > 4:
                self.strand = spln[5]

    def __init__(self, bed_text):
        """
        :param bed_file_name:
        :return:
        """
        self.bed_list = []
        for line in bed_text.split("\n"):
            self.bed_list.append(self.Feature(line))
