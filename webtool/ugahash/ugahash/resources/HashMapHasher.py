from hashlib import md5
import base64

def base36encode(number, alphabet='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
    """Converts an integer to a base36 string."""
    if not isinstance(number, (int, long)):
        raise TypeError('number must be an integer')

    base36 = ''
    sign = ''

    if number < 0:
        sign = '-'
        number = -number

    if 0 <= number < len(alphabet):
        return sign + alphabet[number]

    while number != 0:
        number, i = divmod(number, len(alphabet))
        base36 = alphabet[i] + base36

    return sign + base36

def base36decode(number):
    return int(number, 36)


def base58encode(num, alphabet='0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'):
    """ Returns num in a base58-encoded string """
    encode = ''
    base_count = len(alphabet)

    if (num < 0):
        return ''
    while (num >= base_count):
        mod = num % base_count
        encode = alphabet[mod] + encode
        num = num / base_count
    if (num):
        encode = alphabet[num] + encode
    return encode

def base58decode(s, alphabet='0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'):
    """ Decodes the base58-encoded string s into an integer """
    decoded = 0
    base_count = len(alphabet)
    multi = 1
    s = s[::-1]
    for char in s:
        decoded += multi * alphabet.index(char)
    multi = multi * base_count
    return decoded


def namenovelgene(species_two_letter_code, assembly, tax_id, chromosome, strand, start, end):
    #Patch chromosomes have underscores
    #c1gCHR_HG2217_PATCH-234151-254174
    m = md5(assembly+"_"+tax_id+"_"+chromosome+"_"+strand+base36encode(int(start))+"-"+base36encode(int(end)))
    unique_str = m.hexdigest()
    return "CN1G"+species_two_letter_code+unique_str


def namenoveltranscript(species_two_letter_code, assembly, tax_id, chromosome, strand, exon_start_end_list):
    x=0
    #Patch chromosomes have underscores
    #c1tCHR_HG2217_PATCH-234151-254174:234151-254174:234151-254174
    exon_list = []
    for start, end in exon_start_end_list:
        bstart = base36encode( int(start))
        bend = base36encode(int(end))
        exon_list.append(bstart+"-"+bend)

    m = md5(assembly+"_"+tax_id+"_"+chromosome+strand+":".join(exon_list))
    unique_str = m.hexdigest()
    return "CN1T"+species_two_letter_code+unique_str


def nameexon(chromosome, strand, start, end):
    #c1eCHR_HG2217_PATCH-234151-254174:234151-254174:234151-254174
    return "CN1E"+chromosome+strand+base36encode(start)+"-"+base36encode(end)


def namenovelgene(species_two_letter_code, assembly, tax_id, chromosome, strand, start, end):
    #Patch chromosomes have underscores
    #c1gCHR_HG2217_PATCH-234151-254174
    m = md5(assembly+"_"+tax_id+"_"+chromosome+"_"+strand+base36encode(int(start))+"-"+base36encode(int(end)))
    unique_str = m.hexdigest()
    return "CN1G"+species_two_letter_code+unique_str


"""
def UGAHash(seq_type, tax_id, assembly_version, chromosome, start_end_list, strand):
    #zlib.crc32("tylerxw")
    #resolve_seq_type
    #Resolve seq
    #resolve strand
    longest_tax_id = 5

    seq_type_symbol = seq_type

    if seq_type.lower() == "gene":
        seq_type_symbol = "G"
    elif seq_type.lower() == "transcript":
        seq_type_symbol = "T"
    elif seq_type.lower() == "exon":
        seq_type_symbol = "E"

    assert seq_type_symbol in ("G", "T", "E"), seq_type_symbol

    while len(tax_id) < longest_tax_id:
        tax_id = "0"+tax_id

    bound_list = []

    for start, end in start_end_list:
        start, end = sorted([int(start), int(end)])
        bound_list.append(str(start)+"-"+str(end))

    bounds = ":".join(bound_list)

    md5_obj = md5(assembly_version+"_"+"_"+chromosome+"_"+strand+"_"+bounds)

    md5_digest = md5_obj.hexdigest()

    base64_endoced_md5_digest = md5_digest

    #1+1+5+32
    return "C"+seq_type_symbol+tax_id+base64_endoced_md5_digest
"""


def UGAHash(seq_type, assembly, chromosome, strand, start_end_list):
    #http://crypto.stackexchange.com/questions/231/change-in-probability-of-collision-when-removing-digits-from-md5-hexadecimal-has
    ugahash_tag = "U"
    field_separator = "_"
    bound_list = []
    #print(seq_type, assembly, chromosome, strand, start_end_list)
    chromosome = chromosome.strip("chr")
    prev_start, prev_stop = None, None
    for start, stop in start_end_list:
        tmp_start, tmp_stop = sorted([int(start), int(stop)])
        assert ((prev_start is None or prev_start < tmp_start) ), start_end_list #and (prev_stop is None or prev_stop < tmp_stop)
        bound_list.append(str(tmp_start)+"-"+str(tmp_stop))
        prev_start, prev_stop = tmp_start, tmp_stop
    bounds = ":".join(bound_list)
    md5_digest = md5(chromosome+"_"+strand+"_"+bounds).digest()
    base64_endoced_md5_digest = base64.urlsafe_b64encode(md5_digest)[:-2]
    return field_separator.join((ugahash_tag+seq_type, assembly+base64_endoced_md5_digest))



if __name__ == '__main__':
    #Parse bed to database tsv.
    import sys

    source = sys.argv[2]
    tax_id = sys.argv[3]
    assembly = sys.argv[4]
    seq_type = "G"

    for line in open(sys.argv[1]):
        spln = line.split()

        chromosome = spln[0].strip("chr")
        start = spln[1]
        end = spln[2]
        original_id = spln[3]

        strand = "."
        if len(spln) > 4:
            strand = spln[5]

        id = UGAHash(seq_type, tax_id, assembly, chromosome, ((start, end), ), strand)

        print("\t".join((id, original_id, source, tax_id, assembly, chromosome, start, end, strand)))

