# Create your views here.
from django.http import HttpResponse
from django.template import RequestContext, loader
from hasher.models import Sequence, SequenceSetSequenceRelation, SequenceSet
from .forms import IDSearchForm
from ugahash.resources import FormHelpers
import json
import csv

app_name = "idsearch"


def idsearchview(request):

    if request.method == 'GET' and request.GET != {}:

        total_errors = []
        form = IDSearchForm(request.GET, request.FILES)

        if form.is_valid():
            text_input = form.cleaned_data['text_input']
            text_input, error_message = FormHelpers.resolvefileortext(
                text_input,
                'file_input',
                request
            )
            total_errors.append(error_message)

            out_nested_is_list = []

            for line_el in text_input.split():

                sequence_id = line_el.strip()
                tmp_list = []

                original_id_data = SequenceSetSequenceRelation.objects.filter(
                    original_id=sequence_id).order_by('source_version_id_id')

                #.values_list('source_version_id_id', 'hash_id_id')

                if original_id_data:
                    for hash_el in original_id_data:
                        tmp_list.append(
                            {
                                "source_and_version": hash_el.source_version_id_id,
                                "hash_id": hash_el.hash_id_id
                            }
                        )

                out_nested_is_list.append(
                    {
                        "original_id": sequence_id,
                        "hash_id_list": tmp_list
                     }
                )

            template = loader.get_template(app_name+'/idsearch_results.html')
            context = RequestContext(request, {"out_nested_is_list": out_nested_is_list})

            if "format" in request.GET and request.GET["format"] == "json":
                output = json.dumps(out_nested_is_list, indent=4)
                return HttpResponse(output, content_type="application/json")
            elif "format" in request.GET and request.GET["format"] == "tsv":

                response = HttpResponse(content_type='text/plain')
                writer = csv.writer(response, delimiter='\t')
                writer.writerow([
                    "#"+"original_id",
                    "source_and_version",
                    "hash_id"
                ])

                for id_el in out_nested_is_list:
                    for hash_el in id_el["hash_id_list"]:
                        writer.writerow([
                            id_el["original_id"],
                            hash_el["source_and_version"],
                            hash_el["hash_id"]
                        ])
                return response
            else:
                context.update({
                    "json_link": request.build_absolute_uri() + "&format=json",
                    "tsv_link": request.build_absolute_uri() + "&format=tsv"
                })
                # return HttpResponse(template.render(context))
                return HttpResponse(template.render(context))


        else:
            #form = IDSearchForm()
            template = loader.get_template(app_name+'/index.html')
            context = RequestContext(request, {"form": form})
    else:
        form = IDSearchForm()
        context = RequestContext(request, {"form": form})
        template = loader.get_template(app_name+'/index.html')


    return HttpResponse(template.render(context))


#            sequence_url = "sequence/"
#hash_and_original_id_tuple = Sequence.objects.values_list('original_id', 'hash_id').raw(
#    """SELECT id, original_id, hash_id FROM lncRNACoordinateMapper_sequence
#    WHERE hash_id = "%s" OR original_id = "%s";""" %
#    (sequence_id, sequence_id,))
#database_existance_list = []
#for original_id, hash_id in hash_and_original_id_tuple:
#    database_existance_list.append((original_id, hash_id))
#                print(not hash_and_original_id_tuple)