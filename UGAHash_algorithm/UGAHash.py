from hashlib import md5
import base64


def ugahash(seq_type, assembly, chromosome, strand, start_end_list):
    """
    The UGAHash algorithm organized genomic location in a repeatable way and
    generates an MD5 hash encoded in URL safe base 64.

    :param seq_type: The type of sequence of the Hash being generated. G = Gene, T = Transcript, E = Exon,
                     users can create new sequence types as long as their descriptions consist of a-z orA-Z or 0-9.

    :param assembly: The assembly the genomic coordinates were taken from.
    :param chromosome: The chromosome the feature is located on. chr prefixes are not allowed.
    :param strand: The strand the feature is located on. chr prefixes are not allowed. If not strand specific use ".".
    :param start_end_list: A list of the bounds describing the genomic feature.
    :return: A string containing a base 64 encoded md5 sum has missing the final two == characters.
    """
    ugahash_tag = "U"
    sep = "_"
    seq_sep = ":"
    bound_sep = "-"

    # This step is not required, but include to help improve consistency
    chromosome = chromosome.strip("chr")

    # Generate bounds string.
    bound_list = []
    prev_start, prev_stop = None, None
    for start, stop in start_end_list:
        tmp_start, tmp_stop = sorted([int(start), int(stop)])
        assert prev_start is None or prev_start < tmp_start
        bound_list.append(str(tmp_start)+bound_sep+str(tmp_stop))
        prev_start, prev_stop = tmp_start, tmp_stop

    # Note: Make url safe by substituting
    # "-" for "+" and "_" for "/"
    # in the standard Base64 alphabet.
    md5_digest = md5(chromosome+sep+strand+sep+seq_sep.join(bound_list)).digest()

    # The last two characters are always "=" so remove them.
    base64_encoded_md5_digest = base64.urlsafe_b64encode(md5_digest)[:-2]

    return sep.join((ugahash_tag+seq_type, assembly+base64_encoded_md5_digest))


